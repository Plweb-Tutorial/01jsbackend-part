const PORT = process.env.PORT || 3000;

var express = require("express"); 
var app = express();

//respond with "Hello World!" when get request is made to the homepage
app.get("/", function(req, res){
    //傳送回應至客戶端
    res.send ("<h1>_____________<h1>"); 
});

//listen to server by port 3000
app.listen(PORT, function(){
    console.log("Starting the server port " + PORT);
});
  