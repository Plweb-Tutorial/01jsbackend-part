const PORT = process.env.PORT || 3000;

var express = require("express");
var path = require("path");
var app = express();

//use ejs template view engine 
app.set("view engine", "____________");
//ejs頁面存放路徑
app.set("views", path.join(__dirname, 'views'));
app.use(express.static(__dirname + "/public"));

//respond index page when get request is made to the homepage
app.get("/", function(req, res){
  //設定首頁
  res.render("______");
});

//listen to server by port 3000
app.listen(PORT, function(){
  console.log("Starting the server port " + PORT);
});